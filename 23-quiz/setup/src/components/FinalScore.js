import { useGlobalContext } from '../context'

export const FinalScore = () => {

  const { isQuizCompleted, resetQuiz, correctAnswers, questions } = useGlobalContext();

  return (
    <div className={`modal-container ${isQuizCompleted ? 'isOpen' : ''}`}>
      <div className="modal-content">
        <h2>congrats!</h2>
        <p>
          You answered {((correctAnswers / questions.length) * 100).toFixed(0)}% of
          questions correctly
        </p>
        <button className="close-btn" onClick={resetQuiz}>
          play again
        </button>
      </div>
    </div>
  )
}

