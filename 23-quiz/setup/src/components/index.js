export { Select } from './Select';
export { SetupForm } from './SetupForm'
export { FinalScore } from './FinalScore';
export { Loading } from './Loading'
export { Number } from './Number';
export { Quiz } from './Quiz'