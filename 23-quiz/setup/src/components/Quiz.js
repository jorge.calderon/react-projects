import { useGlobalContext } from './../context'
import { useAnswers } from '../hooks/useAnswers';

export const Quiz = () => {
    const { checkAnswer, questions, activeQuestion, correctAnswers, nextQuestion } = useGlobalContext();

    const { question, incorrect_answers, correct_answer } = questions[activeQuestion];

    const { answers } = useAnswers(correct_answer, incorrect_answers)

    return (
        <section className="quiz">
            <p className="correct-answers">
                correct answers : {correctAnswers}/{questions.length}
            </p>
            <article className="container">
                <h2 dangerouslySetInnerHTML={{ __html: question }} />
                <div className="btn-container">
                    {answers.map((answer, index) => (
                        <button
                            key={index}
                            className='answer-btn'
                            onClick={() => checkAnswer(correct_answer === answer)}
                            dangerouslySetInnerHTML={{ __html: answer }}
                        />
                    ))}
                </div>
            </article>
            <button className="next-question" onClick={nextQuestion}>
                next question
            </button>
        </section>
    )
}
