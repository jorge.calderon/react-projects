import React from "react";
import { useGlobalContext } from "../context";
import { Select, Number } from ".";

export const SetupForm = () => {

  const { form, handleChange, handleSubmit } = useGlobalContext();

  return (
    <section className="quiz quiz-small">
      <form className="setup-form" onSubmit={handleSubmit}>

        <Number
          title="Number of questions"
          name="amount"
          value={form.amount}
          onChange={handleChange}
        />

        <Select
          title="Category"
          name="category"
          value={form.category}
          onChange={handleChange}
        >
          <option value="sports">sports</option>
          <option value="history">history</option>
          <option value="politics">politics</option>
        </Select>

        <Select
          title="Difficulty"
          name="difficulty"
          value={form.difficulty}
          onChange={handleChange}
        >
          <option value="easy">easy</option>
          <option value="medium">medium</option>
          <option value="hard">hard</option>
        </Select>

        <button type="submit" className="submit-btn">Start</button>

      </form>
    </section>
  )
}
