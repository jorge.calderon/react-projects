
export const Number = ({ title, name, min, max, value, onChange }) => {
    return (
        <div className="form-control">
            <label htmlFor={name}>{title}</label>
            <input
                type="number"
                name={name}
                className="form-input"
                min={min}
                max={max}
                value={value}
                onChange={onChange}
            />
        </div>
    )
}
