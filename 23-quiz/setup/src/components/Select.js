
export const Select = ({ title, name, value, onChange, children }) => {
    return (
        <div className="form-control">
            <label htmlFor={name}>{title}</label>
            <select
                name={name}
                className="form-input"
                value={value}
                onChange={onChange}
            >
                {children}
            </select>
        </div>
    );
}
