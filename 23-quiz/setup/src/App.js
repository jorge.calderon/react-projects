import React from "react"
import { useGlobalContext } from "./context"
import { FinalScore, Loading, SetupForm, Quiz } from "./components"

function App() {

  const { loading, settingQuiz } = useGlobalContext();

  if (loading) {
    return <Loading />
  }

  return (
    <main>
      {
        settingQuiz ? <SetupForm /> : <Quiz />
      }
      <FinalScore />
    </main>
  )
}

export default App
