import { createContext, useState, useContext } from 'react'
import axios from 'axios'
import { API_ENDPOINT } from './config/api'

const table = {
  sports: 21,
  history: 23,
  politics: 24,
}

const AppContext = createContext()

const AppProvider = ({ children }) => {

  const [questions, setQuestions] = useState([]);
  const [activeQuestion, setActiveQuestion] = useState(0);
  const [correctAnswers, setCorrectAnswers] = useState(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [isQuizCompleted, setIsQuizCompleted] = useState(false);
  const [settingQuiz, setSettingQuiz] = useState(true);
  const [form, setForm] = useState({
    amount: 5,
    category: 'sports',
    difficulty: 'easy',
  });

  const getQuestionsFromAPI = async (url) => {
    setLoading(true)
    setSettingQuiz(false)
    try {
      const response = await axios(url)
      if (response) {
        const data = response.data.results
        console.log(data)
        if (data.length > 0) {
          setQuestions(data)
          setLoading(false)
          setSettingQuiz(false)
          setError(false)
        } else {
          setSettingQuiz(true)
          setError(true)
        }
      } else {
        setSettingQuiz(true)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const nextQuestion = () => {
    setActiveQuestion((oldIndex) => {
      const nextIndex = oldIndex + 1;
      const isLastQuestion = nextIndex > questions.length - 1
      if (isLastQuestion) {
        completeQuiz()
        return 0
      } else {
        return nextIndex
      }
    })
  }

  const checkAnswer = (isCorrect) => {
    if (isCorrect) {
      setCorrectAnswers(correctAnswers + 1)
    }
    nextQuestion()
  }

  const completeQuiz = () => setIsQuizCompleted(true)

  const resetQuiz = () => {
    setSettingQuiz(true)
    setCorrectAnswers(0)
    setIsQuizCompleted(false)
  }

  const handleChange = ({ target }) => {
    const { name, value } = target
    setForm({ ...form, [name]: value })
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const { amount, category, difficulty } = form
    const url = `${API_ENDPOINT}amount=${amount}&difficulty=${difficulty}&category=${table[category]}&type=multiple`
    getQuestionsFromAPI(url)
  }

  return (
    <AppContext.Provider
      value={{
        settingQuiz,
        loading,
        questions,
        activeQuestion,
        correctAnswers,
        error,
        isQuizCompleted,
        nextQuestion,
        checkAnswer,
        resetQuiz,
        form,
        handleChange,
        handleSubmit,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export const useGlobalContext = () => useContext(AppContext)

export { AppContext, AppProvider }
