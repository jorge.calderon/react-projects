export const useAnswers = (correct_answer, incorrect_answers) => {

    let answers = [...incorrect_answers]
    const tempIndex = Math.floor(Math.random() * 4)

    if (tempIndex === 3) {
        answers.push(correct_answer)
    } else {
        answers.push(answers[tempIndex])
        answers[tempIndex] = correct_answer
    }

    return { answers }
}