import React from 'react';

const List = ({ birthdays }) => {
  return (
    <>
      {birthdays.map(({ id, name, age, image }) => (
        <article className='person' key={id}>
          <img src={image} alt={name} />
          <div>
            <h4>{name}</h4>
            <p>{`${age} ${age === 1 ? 'year' : 'years'}`}</p>
          </div>
        </article>
      ))}
    </>
  );
};

export default List;
