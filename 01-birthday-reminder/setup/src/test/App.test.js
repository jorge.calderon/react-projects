import { render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import "@testing-library/jest-dom"
import data from '../data'
import App from '../App'

describe('App test', () => {

    test('should render title and birthdays', () => {
        render(<App />);

        const title = '5 birthdays today';

        expect(screen.getByText(title)).toBeInTheDocument()
        data.forEach(({ name, age }) => {
            expect(screen.getByText(name)).toBeInTheDocument()
            expect(screen.getByText(`${age} years`)).toBeInTheDocument()

        })
    })

    test('should clear all birthdays', () => {
        render(<App />)
        const button = screen.getByRole("button", { name: "Clear all" })

        userEvent.click(button);

        expect(screen.getByText("0 birthdays today")).toBeInTheDocument()
    })
})