import React, { useState } from 'react';
import data from './data';
import List from './List';
function App() {
  const [birthdays, setBirthdays] = useState(data);
  const birthdaysToday = birthdays.length;

  return (
    <main>
      <section className='container'>
        <h3>{`${birthdaysToday} ${birthdays === 1 ? 'birthday' : 'birthdays'} today`}</h3>
        <List birthdays={birthdays} />
        <button onClick={() => setBirthdays([])}> Clear all </button>
      </section>
    </main>
  );
}

export default App;
