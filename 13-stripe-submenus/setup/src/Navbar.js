import React from 'react';
import logo from './images/logo.svg';
import { FaBars } from 'react-icons/fa';
import { useMyContext } from './context';

const links = ['Products', "Developers", "Company"]

const Navbar = () => {

  const { openSidebar, openSubmenu, closeSubmenu } = useMyContext();

  const showSubMenu = ({ target }) => {
    const page = target.textContent;
    const dimentions = target.getBoundingClientRect();
    const center = (dimentions.left + dimentions.right) / 2;
    const bottom = dimentions.bottom - 3;
    openSubmenu(page, { center, bottom });
  };

  const handleSubmenu = ({ target: { classList } }) => {
    if (!classList.contains('link-btn')) {
      closeSubmenu();
    }
  };

  return (
    <nav className="nav" onMouseOver={handleSubmenu}>
      <div className="nav-center">
        <div className="nav-header">
          <img src={logo} className="nav-logo" alt="" />
          <button className="btn toggle-btn" onClick={openSidebar}>
            <FaBars />
          </button>
        </div>
        <ul className="nav-links">
          {
            links.map((link, i) => (
              <li key={i}>
                <button className="link-btn" onMouseOver={showSubMenu}>{link}</button>
              </li>
            ))
          }
        </ul>
        <button className="btn signin-btn">Sign in</button>
      </div>
    </nav>
  );
};

export default Navbar;
