import React, { createContext, useState, useContext } from 'react';
import sublinks from './data';

const AppContext = createContext();

const AppProvider = ({ children }) => {
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);
    const [isSubmenuOpen, setIsSubmenuOpen] = useState(false);
    const [page, setPage] = useState({ page: '', links: [] });
    const [location, setLocation] = useState({});

    const openSidebar = () => setIsSidebarOpen(true);
    const closeSidebar = () => setIsSidebarOpen(false);
    const closeSubmenu = () => setIsSubmenuOpen(false);

    const openSubmenu = (text, coordinates) => {
        const page = sublinks.filter((link) => link.page.toLowerCase() === text.toLowerCase())[0];
        setPage(page);
        setLocation(coordinates);
        setIsSubmenuOpen(true);
    };

    return (
        <AppContext.Provider
            value={{
                isSidebarOpen,
                openSidebar,
                closeSidebar,
                isSubmenuOpen,
                openSubmenu,
                closeSubmenu,
                page,
                location,
            }}
        >
            {children}
        </AppContext.Provider>
    );
};
const useMyContext = () => useContext(AppContext);

export { AppProvider, useMyContext };
