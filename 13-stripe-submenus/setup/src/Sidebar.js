import React from 'react';
import { FaTimes } from 'react-icons/fa';
import { useMyContext } from './context';
import sublinks from './data';

const Sidebar = () => {

  const { isSidebarOpen, closeSidebar } = useMyContext()

  return (
    <div className={`sidebar-wrapper${isSidebarOpen ? " show" : ""}`}>
      <aside className="sidebar">
        <button className="close-btn" onClick={closeSidebar}>
          <FaTimes />
        </button>
        <div className="sidebar-links">
          {sublinks.map(({ links, page }, i) => (
            <article key={i}>
              <h4>{page}</h4>
              <div className="sidebar-sublinks">
                {links.map(({ url, icon, label }, index) => (
                  <a key={index} href={url}>
                    {icon}
                    {label}
                  </a>
                ))}
              </div>
            </article>
          ))}
        </div>
      </aside>
    </div>
  )
}

export default Sidebar
