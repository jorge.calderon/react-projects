import { TYPES } from "./types/types";

const reducer = (state, action) => {
    const { cart } = state;
    switch (action.type) {
        case TYPES.CLEAR_CART:
            return { ...state, cart: [] }
        case TYPES.REMOVE:
            return {
                ...state,
                cart: state.cart.filter((cartItem) => cartItem.id !== action.payload),
            }
        case TYPES.INCREASE:
            return {
                ...state, cart: cart.map((cartItem) => {
                    if (cartItem.id === action.payload) {
                        return { ...cartItem, amount: cartItem.amount + 1 }
                    }
                    return cartItem
                })
            }
        case TYPES.DECREASE:
            return {
                ...state, cart: cart.map((cartItem) => {
                    if (cartItem.id === action.payload) {
                        return { ...cartItem, amount: cartItem.amount + 1 }
                    }
                    return cartItem
                })
            }

        case TYPES.GET_TOTALS:
            let { total, amount } = state.cart.reduce((cartTotal, cartItem) => {
                const { price, amount } = cartItem
                const itemTotal = price * amount

                cartTotal.total += itemTotal
                cartTotal.amount += amount
                return cartTotal
            }, { total: 0, amount: 0, })
            total = parseFloat(total.toFixed(2))

            return { ...state, total, amount }
        case TYPES.LOADING:
            return { ...state, loading: true }
        case TYPES.DISPLAY_ITEMS:
            return { ...state, cart: action.payload, loading: false }
        case TYPES.TOGGLE_AMOUNT:
            return {
                ...state, cart: cart.map((cartItem) => {
                    if (cartItem.id === action.payload.id) {
                        if (action.payload.type === 'inc') {
                            return { ...cartItem, amount: cartItem.amount + 1 }
                        }
                        if (action.payload.type === 'dec') {
                            return { ...cartItem, amount: cartItem.amount - 1 }
                        }
                    }
                    return cartItem
                }).filter((cartItem) => cartItem.amount !== 0)
            }
        default:
            return state;
    }
}

export default reducer;