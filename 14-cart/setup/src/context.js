import React, { createContext, useContext, useReducer, useEffect } from 'react';
import reducer from './reducer';
import { TYPES } from './types/types';
import { URL } from './api/configuration'

const AppContext = createContext()

const initialState = {
  loading: false,
  cart: [],
  total: 0,
  amount: 0,
}

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  const clearCart = () => dispatch({ type: TYPES.CLEAR_CART })

  const remove = (id) => dispatch({ type: TYPES.REMOVE, payload: id })

  const increase = (id) => dispatch({ type: TYPES.INCREASE, payload: id })

  const decrease = (id) => dispatch({ type: TYPES.DECREASE, payload: id })

  const toggleAmount = (id, type) => dispatch({ type: TYPES.TOGGLE_AMOUNT, payload: { id, type } })

  const fetchData = async () => {
    try {
      dispatch({ type: TYPES.LOADING })
      const response = await fetch(URL);
      const cart = await response.json();
      dispatch({ type: TYPES.DISPLAY_ITEMS, payload: cart })
    } catch (error) {
      console.log(error);
      dispatch({ type: TYPES.CLEAR_CART });
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  useEffect(() => {
    dispatch({ type: TYPES.GET_TOTALS })
  }, [state.cart]);

  return (
    <AppContext.Provider
      value={{
        ...state,
        fetchData,
        clearCart,
        remove,
        increase,
        decrease,
        toggleAmount,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export const useMyContext = () => useContext(AppContext)

export { AppContext, AppProvider }
