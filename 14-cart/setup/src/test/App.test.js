import { render, screen, waitForElementToBeRemoved } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { AppProvider } from './../context'
import "@testing-library/jest-dom";
import App from '../App';
import { data } from './data'

describe('App test', () => {

    let originalFetch;

    beforeEach(() => {
        originalFetch = global.fetch;
        global.fetch = jest.fn(() => Promise.resolve({
            json: () => Promise.resolve(data)
        }));
    });

    afterEach(() => {
        global.fetch = originalFetch;
    });

    test('should render the information from the api', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByText("Loading..."))

        data.forEach(({ title, price, amount }) => {
            expect(screen.getByText(title)).toBeInTheDocument()
            expect(screen.getByText(`$${price}`)).toBeInTheDocument()
            expect(screen.getByText(amount)).toBeInTheDocument()
        })
    });

    test('should increase the amount for the last product', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByText("Loading..."))
        const buttons = screen.getAllByRole("button", { name: "increase amount" })
        const lastButton = buttons[buttons.length - 1];
        userEvent.click(lastButton);
        expect(screen.getByText('6')).toBeInTheDocument();

        //Total amount 
        expect(screen.getByText('$8299.86')).toBeInTheDocument();
    });


    test('should decrease the amount for the second product', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByText("Loading..."))
        const buttons = screen.getAllByRole("button", { name: "decrease amount" })
        const secondButton = buttons[1];
        userEvent.click(secondButton);
        expect(screen.getByText('2')).toBeInTheDocument();
    });

    test('should clear cart', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByText("Loading..."))

        const clearCartButton = screen.getByRole("button", { name: "clear cart" })
        userEvent.click(clearCartButton);

        data.forEach(({ title, price, amount }) => {
            expect(screen.queryByText(title)).not.toBeInTheDocument()
            expect(screen.queryByText(`$${price}`)).not.toBeInTheDocument()
            expect(screen.queryByText(amount)).not.toBeInTheDocument()
        })

        expect(screen.getByText("your bag")).toBeInTheDocument()
        expect(screen.getByText("is currently empty")).toBeInTheDocument()

    });
});