import React from 'react'
import Navbar from './Navbar'
import CartContainer from './CartContainer'
import { useMyContext } from './context'

function App() {
  const { loading } = useMyContext();

  if (loading) {
    return (
      <>
        <Navbar />
        <div className="loading">
          <h1>Loading...</h1>
        </div>
      </>
    )
  }
  return (
    <main>
      <Navbar />
      <CartContainer />
    </main>
  )
}

export default App
