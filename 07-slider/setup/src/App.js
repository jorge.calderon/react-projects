import React, { useState, useEffect } from 'react';
import { FiChevronRight, FiChevronLeft } from 'react-icons/fi';
import { FaQuoteRight } from 'react-icons/fa';
import data from './data';
function App() {
  const [active, setActive] = useState(1);

  useEffect(() => {
    if (active > data.length - 1) {
      setActive(0);
    }

    if (active < 0) {
      setActive(data.length - 1);
    }
  }, [active]);

  useEffect(() => {
    const interval = setInterval(() => { setActive(active - 1) }, 1000)
    return () => clearInterval(interval)
  }, [active])


  return (
    <section className='section'>
      <div className="title">
        <h2>
          <span>/</span>
          reviews
        </h2>
      </div>

      <div className="section-center">
        {
          data.map(({ id, image, name, title, quote }, i) => {

            let className = 'nextSlide';

            if (i === active) {
              className = "activeSlide"
            }

            if (active === i - 1 || (i === 0 && active === data.length - 1)) {
              className = 'lastSlide';
            }

            return (
              <article className={className} key={id}>
                <img src={image} alt={name} className="person-img" />
                <h4>{name}</h4>
                <p className="title">{title}</p>
                <p className="text">
                  {quote}
                </p>
                <FaQuoteRight className='icon' />
              </article>
            )
          })
        }
        <button className='prev' onClick={() => setActive((active) => active + 1)}>
          <FiChevronLeft />
        </button>
        <button className='next' onClick={() => setActive((active) => active - 1)}>
          <FiChevronRight />
        </button>
      </div>

    </section>
  );
}

export default App;
