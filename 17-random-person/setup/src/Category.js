import React from 'react'

export const Category = ({ dataLabel, onMouseOver, children }) => {
    return (
        <button className="icon" data-label={dataLabel} onMouseOver={onMouseOver}>
            {children}
        </button>
    )
}
