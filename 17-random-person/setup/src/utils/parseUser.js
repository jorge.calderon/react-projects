export const parseUser = (user) => {
    const { phone, email } = user
    const { large: image } = user.picture
    const { password } = user.login
    const { first, last } = user.name
    const {
        dob: { age },
    } = user
    const {
        street: { number, name },
    } = user.location

    const newPerson = {
        image,
        phone,
        email,
        password,
        age,
        street: `${number} ${name}`,
        name: `${first} ${last}`,
    }

    return newPerson;
}