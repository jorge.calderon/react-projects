import {
    FaEnvelopeOpen,
    FaUser,
    FaCalendarTimes,
    FaMap,
    FaPhone,
    FaLock,
} from 'react-icons/fa';


export const categories = [
    {
        name: "name",
        icon: <FaUser />
    },
    {
        name: "email",
        icon: <FaEnvelopeOpen />
    },
    {
        name: "age",
        icon: <FaCalendarTimes />
    },
    {
        name: "street",
        icon: <FaMap />
    },
    {
        name: "phone",
        icon: <FaPhone />
    },
    {
        name: "password",
        icon: <FaLock />
    },


]
