import React, { useState } from 'react'
import { categories } from './categories';
import { Category } from './Category';
import { useGetUsers } from './hooks/useGetUsers';

const defaultImage = 'https://www.nabeeats.com/assets/images/default-user.png';

function App() {
  const [activeField, setActiveField] = useState("name");
  const [value, setValue] = useState("")
  const { loading, user, fetchUser } = useGetUsers(setValue)


  const handleValue = (e) => {
    if (e.target.classList.contains('icon')) {
      const newValue = e.target.dataset.label
      setActiveField(newValue)
      setValue(user[newValue])
    }
  }

  return (
    <main>
      <div className="block bcg-black" />
      <div className="block">
        <div className="container">
          <img src={loading ? defaultImage : user.image} alt="random user" className='user-img' />
          <p className='user-title'>{`Mi ${activeField} is`}</p>
          <p className='user-value'>{value}</p>
          <div className="values-list">
            {
              categories.map(({ name, icon }, i) =>
                <Category key={i} dataLabel={name} onMouseOver={handleValue}>
                  {icon}
                </Category>
              )
            }
          </div>
          <button className="btn" onClick={fetchUser}>{loading ? "Loading..." : "Get random user"}</button>
        </div>
      </div>
    </main>
  )
}

export default App
