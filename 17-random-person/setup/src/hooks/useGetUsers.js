import { useState, useEffect, useCallback } from 'react';
import { parseUser } from '../utils/parseUser';

const API = 'https://randomuser.me/api/';

export const useGetUsers = (setValue) => {
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useState({});

    const fetchUser = useCallback(() => {
        setLoading(true)
        fetch(API)
            .then((resp) => resp.json())
            .then((data) => {
                const user = parseUser(data.results[0]);
                setValue(user["name"])
                setUser(user)
            }).finally(() => setLoading(false))
    }, [setValue])

    useEffect(() => {
        fetchUser()
    }, [fetchUser])

    return {
        loading,
        user,
        fetchUser
    }
}