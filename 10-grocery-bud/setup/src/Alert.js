import React from 'react'
import { ALERT_TYPE } from './hooks/useAlert'

const Alert = ({ msg, type = ALERT_TYPE.SUCCESS }) => {
  return <p className={`alert alert-${type}`}>{msg}</p>
}

export default Alert
