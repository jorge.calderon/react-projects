import { useState, useEffect } from 'react';

export const ALERT_TYPE = {
    SUCCESS: "success",
    DANGER: "danger",
}

const initiState = { open: false, msg: "", type: "" }

export const useAlert = () => {
    const [alert, setAlert] = useState(initiState);

    const openAlert = ({ msg, type = ALERT_TYPE.SUCCESS }) => setAlert({ open: true, msg, type })

    useEffect(() => {
        const interval = setInterval(() => setAlert(initiState), 1500);
        return () => clearInterval(interval)
    }, [alert]);

    return {
        openAlert,
        alert
    }

}