import React from 'react'
import { FaEdit, FaTrash } from 'react-icons/fa'

const List = ({ list, handleEdit, handleDelete }) => {

  return <div className="grocery-list">
    {
      list.map(({ grocery, id }) => (
        <article key={id} className="grocery-item">
          <p className="title">{grocery}</p>
          <div className="btn-container">
            <button className="edit-btn" aria-label="edit" onClick={() => handleEdit({ grocery, id })}><FaEdit /></button>
            <button className="delete-btn" aria-label="delete" onClick={() => handleDelete(id)}>
              <FaTrash />
            </button>
          </div>
        </article>
      ))
    }
  </div>
}

export default List
