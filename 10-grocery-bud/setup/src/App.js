import React, { useState, useEffect, useRef } from 'react'
import { ALERT_TYPE, useAlert } from './hooks/useAlert'
import List from './List'
import Alert from './Alert'


const initialFormState = { editing: false, id: null };
const GROCERIES = 'groceries';

function App() {

  const [grocery, setGrocery] = useState("");
  const [groceries, setGroceries] = useState(JSON.parse(localStorage.getItem(GROCERIES)) || []);
  const [formMode, setFormMode] = useState(initialFormState);
  const inputRef = useRef(null);
  const { alert, openAlert } = useAlert()

  const handleSubmit = (e) => {
    e.preventDefault();
    if (formMode.editing) {
      setGroceries((prevState) => prevState.map((prevGrocery) => prevGrocery.id === formMode.id ? { id: prevGrocery.id, grocery } : prevGrocery));
      setFormMode(initialFormState);
      openAlert({ msg: "Value Changed" })
    } else {
      setGroceries((prevState) => [...prevState, { id: Date.now(), grocery }])
      openAlert({ msg: "Item Added To The List" });
    }
    setGrocery("");
  }

  const handleEdit = ({ grocery, id }) => {
    setFormMode({ editing: true, id });
    setGrocery(grocery);
    inputRef.current.focus();
  }

  const handleDelete = (id) => {
    setGroceries((prevState) => prevState.filter((g) => g.id !== id))
    openAlert({ msg: "Item Removed", type: ALERT_TYPE.DANGER })
  }

  const handleClear = () => {
    setGroceries([])
    openAlert({ msg: "Empty List", type: ALERT_TYPE.DANGER })
  }

  useEffect(() => {
    localStorage.setItem(GROCERIES, JSON.stringify(groceries));
  }, [groceries]);

  return (
    <section className="section-center">
      <form className="grocery-form" onSubmit={handleSubmit}>
        {
          alert.open && <Alert {...alert} />
        }
        <h3>Grocery bud</h3>
        <div className="form-control">
          <input type="text" className="grocery" ref={inputRef} placeholder="e.g. pancakes" value={grocery} onChange={({ target: { value } }) => setGrocery(value)} />
          <button type="submit" className="submit-btn" >
            {formMode.editing ? "Edit" : "Submit"}
          </button>
        </div>
      </form>
      <div className="grocery-container">
        <List list={groceries} handleEdit={handleEdit} handleDelete={handleDelete} />
        {
          groceries.length > 0 && <button className="clear-btn" onClick={handleClear}>Clear items</button>
        }
      </div>
    </section>
  )
}

export default App
