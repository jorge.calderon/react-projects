import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from './App';

describe('App test', () => {

    test('should add a grocery', () => {

        render(<App />);

        const input = screen.getByRole("textbox");
        userEvent.type(input, "pancakes");

        const submitButton = screen.getByRole("button", { name: "Submit" });

        userEvent.click(submitButton);
        expect(screen.getByText("pancakes")).toBeInTheDocument()
        expect(screen.getByText("Item Added To The List")).toBeInTheDocument()

    });

    test('should delete a grocery', () => {

        render(<App />);

        const deleteGrocery = screen.getByRole("button", { name: "delete" });

        userEvent.click(deleteGrocery);
        expect(screen.queryByText("pancakes")).not.toBeInTheDocument();
        expect(screen.getByText("Item Removed")).toBeInTheDocument()
    });

    test('should edit a grocery', () => {

        render(<App />);

        const input = screen.getByRole("textbox");
        userEvent.type(input, "eggs");

        const submitButton = screen.getByRole("button", { name: "Submit" });

        userEvent.click(submitButton);

        const editGrocery = screen.getByRole("button", { name: "edit" });

        userEvent.click(editGrocery);

        userEvent.type(input, " and milk");

        userEvent.click(submitButton);

        expect(screen.getByText("eggs and milk")).toBeInTheDocument();

        expect(screen.getByText("Value Changed")).toBeInTheDocument();
    });

    test('should clear all a groceries', () => {

        render(<App />);

        const clearButton = screen.getByRole("button", { name: "Clear items" });

        userEvent.click(clearButton)

        expect(screen.queryByText("eggs and milk")).not.toBeInTheDocument();

        expect(screen.getByText("Empty List")).toBeInTheDocument();
    });

})