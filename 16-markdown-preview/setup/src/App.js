import React, { useState } from 'react'
import ReactMarkdown from 'react-markdown'

function App() {
  const [input, setInput] = useState("")

  return (
    <main>
      <section className='markdown'>
        <textarea className='input' name="textArea" id="textArea" cols="30" rows="10" value={input} onChange={(e) => { setInput(e.target.value) }} />
        <article className='result'>
          <ReactMarkdown children={input} />
        </article>
      </section>
    </main>
  )
}

export default App
