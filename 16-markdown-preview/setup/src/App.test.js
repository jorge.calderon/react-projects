import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from './App';

describe('App test', () => {

    test('should create a preview of a markdown', () => {

        render(<App />);

        const input = screen.getByRole("textbox");
        userEvent.type(input, `### Hello world {enter}`);
        userEvent.type(input, "[[This is a link](https://www.google.com)");


        expect(screen.getByText("Hello world")).toBeInTheDocument();
        expect(screen.queryByText("http://www.google.com")).not.toBeInTheDocument();

        expect(screen.getByText("This is a link")).toBeInTheDocument();


        const link = screen.getByRole("link");
        expect(link.textContent).toEqual("This is a link");
        expect(link.href).toContain("https://www.google.com");


    });

})