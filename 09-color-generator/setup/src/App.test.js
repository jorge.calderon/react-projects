import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from './App';

const LIGHT_PINK_PALETTE = ['#ffffff', "#fff0f3", "#ffe2e6", "#ffd3da", "#ffc5cd", "#ffb6c1", "#cc929a", "#996d74", "#66494d", "#332427", "#000000"]

Object.assign(navigator, {
    clipboard: {
        writeText: () => { },
    },
});

describe('App test', () => {

    test('should render light pink palette', () => {

        render(<App />);

        const input = screen.getByRole("textbox");
        userEvent.type(input, "lightpink");

        const generateButton = screen.getByRole("button", { name: "Submit" });
        userEvent.click(generateButton);

        LIGHT_PINK_PALETTE.forEach((color) => {
            expect(screen.getByText(color)).toBeInTheDocument()
        })

    });

    test('should copy to the clipboard one color', () => {
        jest.spyOn(navigator.clipboard, "writeText");

        const WHITE = "#ffffff"
        render(<App />)

        const color = screen.getByText(WHITE);

        userEvent.click(color)
        expect(navigator.clipboard.writeText).toHaveBeenCalledWith(WHITE);

        expect(screen.getByText("Copied to clipboard")).toBeInTheDocument()

    });

})