import React, { useState, useEffect } from 'react'

const SingleColor = ({ index, color, percentage }) => {

  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    const interval = setInterval(() => {
      setShowAlert(false)
    }, 2500);

    return () => clearInterval(interval)

  }, [showAlert])

  return <article
    className={`color ${index > 5 ? 'color-light' : ''}`}
    style={{ backgroundColor: color }}
    onClick={() => {
      navigator.clipboard.writeText(color);
      setShowAlert(true);
    }}
  >
    <p className="percent-value">
      {`${percentage}%`}
    </p>
    <p className="color-value">
      {color}
    </p>
    {
      showAlert && <p className="alert">Copied to clipboard</p>
    }
  </article>
}

export default SingleColor
