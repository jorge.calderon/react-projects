import Values from 'values.js'

export const getColors = (color) => new Values(color).all(20)
