import React, { useState } from 'react'
import Form from './Form'
import SingleColor from './SingleColor';
import { getColors } from './utils';


function App() {

  const [colors, setColors] = useState(getColors('lightblue'));

  return (
    <>
      <section className="container">
        <h3>Color Generator</h3>
        <Form setColors={setColors} />
      </section>
      <section className="colors">
        {
          colors.map((color, index) => <SingleColor index={index} key={index} color={color.hexString()} percentage={color.weight} />)
        }
      </section>
    </>
  )
}

export default App
