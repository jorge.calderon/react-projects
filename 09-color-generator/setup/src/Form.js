import { useState } from 'react'
import { getColors } from './utils';

const Form = ({ setColors }) => {
    const [color, setColor] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        try {
            const colors = getColors(color)
            setColors(colors)
        } catch (_) {
            alert("Color not recognized, please try again");
            setColor("");
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <input type="text" placeholder="lightblue or #FFFFF" value={color} onChange={(e) => setColor(e.target.value)} />
            <button className="btn">Submit</button>
        </form>
    )
}

export default Form