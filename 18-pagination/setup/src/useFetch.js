import { useState, useEffect, useCallback } from 'react'
import { API } from './configuration/api'
import paginate from './utils'

export const useFetch = () => {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState([])
  const [numOfPages, setNumOfPages] = useState(0);

  const getProducts = useCallback(async () => {
    try {
      const response = await fetch(API)
      const data = await response.json();
      const pagination = paginate(data, 10)
      setData(pagination);
      setNumOfPages(pagination.length)
    } catch (error) {
      console.log(error)
      alert("Something went wrong, please try again")
    } finally {
      setLoading(false)
    }
  }, [])

  useEffect(() => {
    getProducts()
  }, [getProducts])

  return { loading, data, numOfPages }
}
