import { render, screen, waitForElementToBeRemoved } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from '../App';
import { followers } from './data'

describe('App test', () => {

    let originalFetch;

    beforeEach(() => {
        originalFetch = global.fetch;
        global.fetch = jest.fn(() => Promise.resolve({
            json: () => Promise.resolve(followers)
        }));
    });

    afterEach(() => {
        global.fetch = originalFetch;
    });


    test('should render a second page of followers', async () => {

        render(<App />);

        await waitForElementToBeRemoved(() => screen.getByText("Loading"));

        const secondPageOfFollowers = followers.slice(10, 20);

        const secondPageButton = screen.getByRole("button", { name: "2" });

        userEvent.click(secondPageButton);

        secondPageOfFollowers.forEach(({ login }) => {
            expect(screen.getByText(`$${login}`)).toBeInTheDocument()
        });

    });


    test('should go to the last page', async () => {

        render(<App />);

        await waitForElementToBeRemoved(() => screen.getByText("Loading"));

        const thirdPageOfFollowers = followers.slice(20, 30);

        const secondPageButton = screen.getByRole("button", { name: "prev" });

        userEvent.click(secondPageButton);

        thirdPageOfFollowers.forEach(({ login }) => {
            expect(screen.getByText(`$${login}`)).toBeInTheDocument()
        });

    });
})