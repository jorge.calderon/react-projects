import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Follower from '../Follower';
import { followers } from './data';

const follower = followers[0];

describe('App test', () => {

    test('should render a card with the follower information', async () => {

        render(<Follower {...follower} />);


        expect(screen.getByText(`$${follower.login}`)).toBeInTheDocument()

        const link = screen.getByRole("link");

        expect(link.textContent).toEqual("view profile");
        expect(link.href).toContain(follower.html_url);

    });
})