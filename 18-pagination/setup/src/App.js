import React, { useState, useEffect } from 'react'
import { useFetch } from './useFetch'
import Follower from './Follower'

function App() {

  const [page, setPage] = useState(0);
  const { loading, data, numOfPages } = useFetch();
  const [users, setUsers] = useState([]);

  const onPageChange = (page) => {
    if (page === -1) {
      return setPage(numOfPages - 1);
    }
    if (page === numOfPages) {
      return setPage(0);
    }
    setPage(page)
  }

  useEffect(() => {
    setUsers(data[page])
  }, [page, data])

  return (
    <main>
      <div className="section-title">
        <h1>pagination</h1>
        <div className="underline" />
      </div>
      <div className="followers">
        <div className="container">
          {
            users?.map((user) => <Follower key={user.id} {...user} />)
          }
        </div>

        {loading ? <p>Loading</p> : (<div className="btn-container">
          <button className='prev-btn' onClick={() => onPageChange(page - 1)}>prev</button>
          {
            [...Array(numOfPages)].map((_, i) =>
            (
              <button
                key={i}
                className={`page-btn ${page === i ? "active-btn" : ""}`}
                onClick={() => setPage(i)}
              >
                {i + 1}
              </button>
            ))
          }
          <button className='next-btn' onClick={() => onPageChange(page + 1)}>next</button>
        </div>
        )}
      </div>
    </main>
  )
}

export default App
