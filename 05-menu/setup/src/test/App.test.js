import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from '../App';
import menu from '../data'

const TYPES_OF_DISHES = {
    BREAKFAST: 'breakfast',
    LUNCH: 'lunch',
    SHAKES: 'shakes'
}

describe('App test', () => {

    test('should render all menu', () => {

        render(<App />);

        menu.forEach(({ title, price, desc }) => {
            expect(screen.getByText(title)).toBeInTheDocument()
            expect(screen.getByText(`$${price}`)).toBeInTheDocument()
            expect(screen.getByText(desc)).toBeInTheDocument()
        })
    })

    test('should render only breakfasts ', () => {

        render(<App />);

        const breakfasts = menu.filter(({ category }) => category === TYPES_OF_DISHES.BREAKFAST)

        const breakfastButton = screen.getByRole("button", { name: TYPES_OF_DISHES.BREAKFAST });

        userEvent.click(breakfastButton);

        breakfasts.forEach(({ title, price, desc }) => {
            expect(screen.getByText(title)).toBeInTheDocument()
            expect(screen.getByText(`$${price}`)).toBeInTheDocument()
            expect(screen.getByText(desc)).toBeInTheDocument()
        })
    })


    test('should render only lunchs ', () => {

        render(<App />);

        const lunchs = menu.filter(({ category }) => category === TYPES_OF_DISHES.LUNCH)

        const lunchButton = screen.getByRole("button", { name: TYPES_OF_DISHES.LUNCH });

        userEvent.click(lunchButton);

        lunchs.forEach(({ title, price, desc }) => {
            expect(screen.getByText(title)).toBeInTheDocument()
            expect(screen.getByText(`$${price}`)).toBeInTheDocument()
            expect(screen.getByText(desc)).toBeInTheDocument()
        })
    })

    test('should render only shakes ', () => {

        render(<App />);

        const shakes = menu.filter(({ category }) => category === TYPES_OF_DISHES.SHAKES)

        const shakesButton = screen.getByRole("button", { name: TYPES_OF_DISHES.SHAKES });

        userEvent.click(shakesButton);

        shakes.forEach(({ title, price, desc }) => {
            expect(screen.getByText(title)).toBeInTheDocument()
            expect(screen.getByText(`$${price}`)).toBeInTheDocument()
            expect(screen.getByText(desc)).toBeInTheDocument()
        })
    })

})