import React, { useState } from 'react';
import Title from './Title'
import Menu from './Menu';
import Categories from './Categories';
import items from './data';

const uniqueCategories = items.map(item => item.category).filter((value, index, self) => self.indexOf(value) === index);

function App() {
  const [menuItems, setMenuItems] = useState(items);

  const filterMenuByCategory = (category) => {
    if (category === "all") {
      setMenuItems(items)
    } else {
      const state = items.filter((item) => item.category === category)
      setMenuItems(state)
    }
  }

  return (
    <main>
      <section className='menu section'>
        <Title title={"Our Menu"} />
        <Categories categories={['all', ...uniqueCategories]} filterMenuByCategory={filterMenuByCategory} />
        <Menu categories={menuItems} />
      </section>
    </main>
  );
}

export default App;
