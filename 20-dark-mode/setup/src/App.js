import React from 'react'
import data from './data'
import Article from './Article'
import { useTheme } from './hooks/useTheme'

function App() {

  const { toggleTheme } = useTheme()

  return (
    <main>
      <nav>
        <div className="nav-center">
          <h1>Overreacted</h1>
          <button className="btn" onClick={toggleTheme}>Toggle</button>
        </div>
      </nav>
      <section className="articles">
        {
          data.map((d) => <Article key={d.id}{...d} />)
        }

      </section>
    </main>
  )
}

export default App
