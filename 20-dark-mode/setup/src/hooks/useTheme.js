import { useState, useEffect } from 'react';
import { getStorageTheme } from '../helpers/getStorageTheme';

const LIGHT_THEME = 'light-theme';
const DARK_THEME = 'dark-theme';

export const useTheme = () => {
    const [theme, setTheme] = useState(getStorageTheme());

    const toggleTheme = () => theme === LIGHT_THEME ? setTheme(DARK_THEME) : setTheme(LIGHT_THEME);

    useEffect(() => {
        document.documentElement.className = theme;
        localStorage.setItem('theme', theme);
    }, [theme]);

    return { toggleTheme }
}