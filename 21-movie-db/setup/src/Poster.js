import React from 'react'
import { Link } from 'react-router-dom'

const NO_PICTURE = "https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png";

export const Poster = ({ id, img, title, year }) => {
    return (
        <Link to={`/movies/${id}`} className='movie'>
            <article>
                <img src={img === 'N/A' ? NO_PICTURE : img} alt={title} />
                <div className="movie-info">
                    <h4 className="title">{title}</h4>
                    <p>{year}</p>
                </div>
            </article>
        </Link>
    )
}

