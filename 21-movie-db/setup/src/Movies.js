import React from "react"
import { useGlobalContext } from "./context"
import { Poster } from './Poster'

const Movies = () => {

  const { isLoading, movies } = useGlobalContext();

  if (isLoading) {
    return <div className="loading" />
  }

  return (
    <section className="movies">
      {
        movies.map((movie) => {
          const { imdbID: id, Poster: img, Title: title, Year: year } = movie;
          return <Poster key={id} id={id} img={img} title={title} year={year} />
        })
      }
    </section>
  )
}

export default Movies
