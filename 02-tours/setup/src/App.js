import React from 'react'
import Title from './Title'
import Tours from './Tours'
import { useGetTours } from './hooks/useGetTours';

function App() {

  const { loading, tours, removeTour, getTours } = useGetTours()

  return (
    <main>
      <section>
        <Title loading={loading} getTours={getTours} tours={tours} />
        {
          tours.length > 0 && <Tours tours={tours} removeTour={removeTour} />
        }
      </section>
    </main>
  )
}

export default App
