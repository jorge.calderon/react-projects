import { render, screen, waitForElementToBeRemoved } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from '../App';
import { data } from './data'

describe('App test', () => {

    let originalFetch;

    beforeEach(() => {
        originalFetch = global.fetch;
        global.fetch = jest.fn(() => Promise.resolve({
            json: () => Promise.resolve(data)
        }));
    });

    afterEach(() => {
        global.fetch = originalFetch;
    });

    test('should render the information from the api', async () => {

        render(<App />);

        expect(await screen.findByText("Our Tours")).toBeInTheDocument()

        data.forEach(({ name, price }) => {
            expect(screen.getByText(name)).toBeInTheDocument()
            expect(screen.getByText(price)).toBeInTheDocument()
        })
    })

    test('should show the full description when Read more button is clicked', async () => {
        render(<App />);

        await waitForElementToBeRemoved(() => screen.getByText("loading..."))

        const button = screen.getAllByRole("button", { name: "Read more" })[0];
        userEvent.click(button);
        expect(screen.getByText(data[0].info)).toBeInTheDocument()

    })


    test('should show No tours left as title when all tours were removed', async () => {
        render(<App />);

        await waitForElementToBeRemoved(() => screen.getByText("loading..."))

        const button = screen.getAllByRole("button", { name: "Not interested" })[0];
        userEvent.click(button);
        expect(screen.getByText("No tours left")).toBeInTheDocument()

    })

})