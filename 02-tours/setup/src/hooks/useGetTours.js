import { useState, useEffect } from 'react';

const url = 'https://course-api.com/react-tours-project'

export const useGetTours = () => {
    const [tours, setTours] = useState([]);
    const [loading, setloading] = useState(true);

    const removeTour = (id) => setTours((tours) => tours.filter((tour) => tour.id !== id))

    const getTours = () => {
        setloading(true)
        fetch(url)
            .then((resp) => resp.json())
            .then((data) => setTours(data))
            .catch(() => { alert('Something went wrong, please try again') })
            .finally(() => setloading(false))
    }

    useEffect(() => {
        getTours()
    }, []);

    return {
        tours,
        loading,
        removeTour,
        getTours
    }
}