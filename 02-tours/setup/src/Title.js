import Loading from './Loading'

const Title = ({ loading, tours, getTours }) => {

    const renderTitle = () => {
        if (loading) {
            return <Loading />
        }

        if (tours.length === 0) {
            return (
                <>
                    <h2>No tours left</h2>
                    <button className='btn' onClick={() => { getTours(); }}>Refresh</button>
                </>
            )
        }
        return (
            <>
                <h2>Our Tours</h2>
                <hr className="underline" />
            </>
        )
    }

    return <div className="title">
        {renderTitle()}
    </div>
}

export default Title