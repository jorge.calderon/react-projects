import React from 'react'

const SideBar = ({ jobs, activeJob, onJobClick }) => {
    return (
        <div className="btn-container">
            {
                jobs.map(({ company, id }) => (<button key={id} onClick={() => onJobClick(id)} className={`job-btn ${activeJob.company === company ? 'active-btn' : ''}`}>{company}</button>))
            }
        </div>
    )
}

export default SideBar