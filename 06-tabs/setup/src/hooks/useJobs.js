import { useState, useEffect } from 'react';

const url = 'https://course-api.com/react-tabs-project';

export const useJobs = () => {

    const [loading, setLoading] = useState(true)
    const [jobs, setJobs] = useState([]);
    const [activeJob, setActiveJob] = useState(null)

    useEffect(() => {
        setLoading(true)
        fetch(url).then((resp) => resp.json()).then((data) => {
            setJobs(data);
            setActiveJob(data[0])
        }).finally(() => setLoading(false))
    }, []);

    const onJobClick = (id) => setActiveJob(jobs.find((job) => job.id === id));


    return {
        loading,
        activeJob,
        onJobClick,
        jobs,
    }
}