import React from 'react'
import JobDescription from './JobDescription'
import { useJobs } from './hooks/useJobs'
import SideBar from './SideBar';


function App() {

  const { activeJob, jobs, onJobClick, loading } = useJobs();

  if (loading) {
    return (
      <section className="section loading">
        <h1>Loading...</h1>
      </section>
    )
  }

  return <section className='section'>
    <div className="title">
      <h2>Experience</h2>
      <div className="underline" />
    </div>
    <div className="jobs-center">
      <SideBar jobs={jobs} activeJob={activeJob} onJobClick={onJobClick} />

      {
        activeJob && <JobDescription job={activeJob} />
      }

    </div>
    {
      activeJob && <button className="btn" type="button">More info</button>
    }
  </section>
}

export default App
