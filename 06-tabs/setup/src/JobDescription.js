import React from 'react';
import { FaAngleDoubleRight } from 'react-icons/fa'

const JobDescription = ({ job }) => {
    return (
        <article className="job-info">
            <h3>{job.title}</h3>
            <h4>{job.company}</h4>
            <p className="job-date">{job.dates}</p>

            {
                job.duties.map((duti, i) => (
                    <div className="job-desc" key={`${job.id}-${i}`}>
                        <FaAngleDoubleRight className="job-icon" />
                        <p>{duti}</p>
                    </div>
                ))
            }
        </article>
    )
}

export default JobDescription