export const data = [
    {
        "id": "recAGJfiU4CeaV0HL",
        "order": 3,
        "title": "Full Stack Web Developer",
        "dates": "December 2015 - Present",
        "duties": [
            "Tote bag sartorial mlkshk air plant vinyl banjo lumbersexual poke leggings offal cold-pressed brunch neutra. Hammock photo booth live-edge disrupt.",
            "Post-ironic selvage chambray sartorial freegan meditation. Chambray chartreuse kombucha meditation, man bun four dollar toast street art cloud bread live-edge heirloom.",
            "Butcher drinking vinegar franzen authentic messenger bag copper mug food truck taxidermy. Mumblecore lomo echo park readymade iPhone migas single-origin coffee franzen cloud bread tilde vegan flexitarian."
        ],
        "company": "TOMMY"
    },
    {
        "id": "recIL6mJNfWObonls",
        "order": 2,
        "title": "Front-End Engineer",
        "dates": "May 2015 - December 2015",
        "duties": [
            "Hashtag drinking vinegar scenester mumblecore snackwave four dollar toast, lumbersexual XOXO. Cardigan church-key pabst, biodiesel vexillologist viral squid.",
            "Franzen af pitchfork, mumblecore try-hard kogi XOXO roof party la croix cardigan neutra retro tattooed copper mug. Meditation lomo biodiesel scenester",
            "Fam VHS enamel pin try-hard echo park raw denim unicorn fanny pack vape authentic. Helvetica fixie church-key, small batch jianbing messenger bag scenester +1",
        ],
        "company": "BIGDROP"
    }
]