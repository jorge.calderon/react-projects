import { render, screen, waitForElementToBeRemoved } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from './../App'
import { data } from './data'

describe('App test', () => {

    let originalFetch;

    beforeEach(() => {
        originalFetch = global.fetch;
        global.fetch = jest.fn(() => Promise.resolve({
            json: () => Promise.resolve(data)
        }));
    });

    afterEach(() => {
        global.fetch = originalFetch;
    });

    test('should render information from the api', async () => {

        render(<App />);

        await waitForElementToBeRemoved(() => screen.getByText("Loading..."));

        data.forEach(({ company }, i) => {
            if (i === 0) {
                expect(screen.getAllByText(company)).toHaveLength(2)
            } else {
                expect(screen.getByText(company)).toBeInTheDocument()
            }
        });

        const { title, dates, duties } = data[0];

        expect(screen.getByText(title)).toBeInTheDocument();
        expect(screen.getByText(dates)).toBeInTheDocument();

        duties.forEach((duti) => {
            expect(screen.getByText(duti)).toBeInTheDocument();
        });
    })


    test('should render experience of the second company', async () => {

        render(<App />);

        await waitForElementToBeRemoved(() => screen.getByText("Loading..."));

        const secondCompany = screen.getByRole("button", { name: "BIGDROP" });

        userEvent.click(secondCompany);

        const { title, dates, duties, company } = data[1];

        expect(screen.getAllByText(company)).toHaveLength(2);

        expect(screen.getByText(title)).toBeInTheDocument();
        expect(screen.getByText(dates)).toBeInTheDocument();

        duties.forEach((duti) => {
            expect(screen.getByText(duti)).toBeInTheDocument();
        });
    });
})