import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from '../App';
import questions from '../data'

describe('App test', () => {

    test('should render information when see more button is clicked', () => {

        render(<App />);

        const firstAnswer = questions[0].info;
        expect(screen.queryByText(firstAnswer)).not.toBeInTheDocument();


        const firstButton = screen.getAllByRole("button")[0]
        userEvent.click(firstButton);

        expect(screen.getByText(firstAnswer)).toBeInTheDocument()

    })

})