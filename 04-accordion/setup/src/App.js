import React from 'react';
import data from './data';
import SingleQuestion from './Question';

function App() {

  return (
    <main>
      <div className='container'>
        <h3>Questions and answers about login</h3>
        <section className='info'>
          {data.map(({ info, title, id }) => <SingleQuestion key={id} info={info} title={title} />)}
        </section>

      </div>
    </main>
  );
}

export default App;
