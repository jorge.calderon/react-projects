import { ACTIONS, PAGE_ACTIONS } from './actions'

const reducer = (state, action) => {

  switch (action.type) {
    case ACTIONS.SET_LOADING:
      return { ...state, isLoading: true };

    case ACTIONS.HANDLE_SEARCH:
      return { ...state, query: action.payload };

    case ACTIONS.SET_STORIES:
      return {
        ...state,
        isLoading: false,
        hits: action.payload.hits,
        nbPages: action.payload.nbPages,
      }

    case ACTIONS.REMOVE_STORY:
      return {
        ...state,
        hits: state.hits.filter((hit) => hit.objectID !== action.payload)
      }

    case ACTIONS.HANDLE_PAGE:
      if (action.payload === PAGE_ACTIONS.INCREMENT) {
        let nextPage = state.page + 1
        if (nextPage > state.nbPages - 1) {
          nextPage = 0
        }
        return { ...state, page: nextPage }
      }

      let prevPage = state.page - 1
      if (prevPage < 0) {
        prevPage = state.nbPages - 1
      }
      return { ...state, page: prevPage }

    default:
      return { ...state }
  }
}
export default reducer
