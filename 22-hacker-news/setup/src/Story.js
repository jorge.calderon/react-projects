import { useGlobalContext } from './context'

export const Story = ({ objectID, title, points, author, num_comments, url }) => {

    const { removeStory } = useGlobalContext()

    return (
        <article className='story'>
            <h4 className='title'>{title}</h4>
            <p className='info'>
                {points} points by <span>{author} | </span> {num_comments}{' '}
                comments
            </p>
            <div>
                <a href={url} className='read-link' target='_blank' rel='noopener noreferrer'>
                    read more
                </a>
                <button className='remove-btn' onClick={() => removeStory(objectID)}>
                    remove
                </button>
            </div>
        </article>
    )
}
