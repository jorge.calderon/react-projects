import React from 'react'
import { useGlobalContext } from './context'

const SearchForm = () => {

  const state = useGlobalContext()

  return (
    <form className="search-form">
      <h2>Search hacker news</h2>
      <input type="text" className="form-input" value={state.query} onChange={({ target }) => { state.handleSearch(target.value) }} />
    </form>
  )
}

export default SearchForm
