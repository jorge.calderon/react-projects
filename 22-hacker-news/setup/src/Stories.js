import { useGlobalContext } from './context'
import { Story } from './Story'

const Stories = () => {

  const { hits, isLoading } = useGlobalContext();

  if (isLoading) {
    return <div className='loading' />
  }

  return (
    <section className="stories">
      {
        hits.map((hit) => (<Story key={hit.objectID} {...hit} />))
      }
    </section>
  )
}

export default Stories
