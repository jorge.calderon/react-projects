import { useContext, useEffect, useReducer, createContext } from "react"

import { ACTIONS } from "./actions";
import { API_ENDPOINT } from "./configuration/api";
import reducer from "./reducer";

const initialState = {
  isLoading: true,
  hits: [],
  query: 'react',
  page: 0,
  nbPages: 0,
}

const AppContext = createContext()

const AppProvider = ({ children }) => {

  const [state, dispatch] = useReducer(reducer, initialState)

  const fetchStories = async (url) => {
    dispatch({ type: ACTIONS.SET_LOADING })
    try {
      const response = await fetch(url)
      const data = await response.json()
      dispatch({
        type: ACTIONS.SET_STORIES,
        payload: { hits: data.hits, nbPages: data.nbPages },
      })
    } catch (error) {
      console.log(error)
    }
  }

  const handleSearch = (query) => dispatch({ type: ACTIONS.HANDLE_SEARCH, payload: query })

  const handlePage = (value) => dispatch({ type: ACTIONS.HANDLE_PAGE, payload: value })

  const removeStory = (id) => dispatch({ type: ACTIONS.REMOVE_STORY, payload: id })

  useEffect(() => {
    fetchStories(`${API_ENDPOINT}query=${state.query}&page=${state.page}`)
  }, [state.query, state.page])

  return (
    <AppContext.Provider value={{ ...state, removeStory, handleSearch, handlePage }}>
      {children}
    </AppContext.Provider>
  )
}

export const useGlobalContext = () => useContext(AppContext)

export { AppContext, AppProvider }
