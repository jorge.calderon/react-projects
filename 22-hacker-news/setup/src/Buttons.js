import React from 'react'
import { PAGE_ACTIONS } from './actions'
import { useGlobalContext } from './context'

const Buttons = () => {

  const { isLoading, page, nbPages, handlePage } = useGlobalContext()
  return (
    <div className="btn-container">
      <button disabled={isLoading} onClick={() => handlePage(PAGE_ACTIONS.DECREMENT)}>prev</button>
      <p>
        {page + 1} of {nbPages}
      </p>
      <button disabled={isLoading} onClick={() => handlePage(PAGE_ACTIONS.INCREMENT)}>next</button>
    </div>
  )
}

export default Buttons
