import { createContext, useState, useContext } from 'react'

const AppContext = createContext();

const AppProvider = ({ children }) => {

    const [sideBar, setSideBar] = useState(false);
    const [modal, setModal] = useState(false);


    const openSideBar = () => setSideBar(true);
    const closeSideBar = () => setSideBar(false);
    const openModal = () => setModal(true);
    const closeModal = () => setModal(false);

    return (
        <AppContext.Provider value={{
            isSidebarOpen: sideBar,
            isModalOpen: modal,
            openModal,
            closeModal,
            openSideBar,
            closeSideBar
        }}>
            {children}
        </AppContext.Provider>
    )

};

const useAppContext = () => useContext(AppContext);

export {
    useAppContext,
    AppProvider
}