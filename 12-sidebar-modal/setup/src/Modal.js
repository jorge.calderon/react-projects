import React from 'react'
import { FaTimes } from 'react-icons/fa';
import { useAppContext } from './context';

const Modal = () => {

  const { isModalOpen, closeModal } = useAppContext()

  if (isModalOpen) {
    return (
      <div className={`modal-overlay show-modal`}>
        <div className="modal-container">
          <h3>Modal opened</h3>
          <button className="close-modal-btn" aria-label="close modal" onClick={closeModal}>
            <FaTimes />
          </button>
        </div>
      </div>
    )
  }

}

export default Modal
