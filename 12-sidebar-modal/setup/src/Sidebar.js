import React from 'react'
import logo from './logo.svg'
import { FaTimes } from 'react-icons/fa'
import { social, links } from './data'
import { useAppContext } from './context'

const Sidebar = () => {
  const { isSidebarOpen, closeSideBar } = useAppContext()

  if (isSidebarOpen) {
    return (
      <aside className={`sidebar show-sidebar`}>

        <div className="sidebar-header">
          <img src={logo} alt="Logo" />
          <button className="close-btn" aria-label="close sidebar" onClick={closeSideBar}>
            <FaTimes />
          </button>
        </div>
        <ul className="links">
          {
            links.map(({ id, url, text, icon }) => (
              <li key={id}>
                <a href={url}>
                  {icon}
                  {text}
                </a>
              </li>
            ))
          }
        </ul>
        <ul className="social-icons">
          {
            social.map(({ id, url, icon }) => (
              <li key={id}>
                <a href={url}>
                  {icon}
                </a>
              </li>
            ))
          }
        </ul>
      </aside>
    )
  }
}

export default Sidebar
