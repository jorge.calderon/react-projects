import React from 'react'
import { useAppContext } from './context'
import { FaBars } from 'react-icons/fa'

const Home = () => {

  const { openSideBar, openModal } = useAppContext();

  return (
    <main>
      <button className="sidebar-toggle" aria-label="open sidebar" onClick={openSideBar}>
        <FaBars />
      </button>
      <button className="btn" onClick={openModal}>Show modal</button>
    </main>
  )
}

export default Home
