
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from './App';

describe('App test', () => {

    test('should open and close modal', () => {

        render(<App />);

        const openModalButton = screen.getByRole("button", { name: "Show modal" });

        userEvent.click(openModalButton);

        expect(screen.getByText("Modal opened")).toBeInTheDocument();

        const closeModalButton = screen.getByRole("button", { name: "close modal" })

        userEvent.click(closeModalButton);

        expect(screen.queryByText('Modal opened')).not.toBeInTheDocument();

    });

    test('should open and close sidebar', () => {

        render(<App />);

        const openModalButton = screen.getByRole("button", { name: "open sidebar" });

        userEvent.click(openModalButton);

        expect(screen.getByText("home")).toBeInTheDocument();

        const closeModalButton = screen.getByRole("button", { name: "close sidebar" })

        userEvent.click(closeModalButton);

        expect(screen.queryByText("home")).not.toBeInTheDocument();

    });
})