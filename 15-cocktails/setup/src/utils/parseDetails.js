export const parseDetails = (drinks) => {
    if (drinks) {

        const {
            strDrink: name,
            strDrinkThumb: image,
            strAlcoholic: info,
            strCategory: category,
            strGlass: glass,
            strInstructions: instructions,
            strIngredient1,
            strIngredient2,
            strIngredient3,
            strIngredient4,
            strIngredient5,
        } = drinks[0]

        let ingredients = [
            strIngredient1,
            strIngredient2,
            strIngredient3,
            strIngredient4,
            strIngredient5,
        ].filter(ingredient => ingredient);

        const newCocktail = {
            name,
            image,
            info,
            category,
            glass,
            instructions,
            ingredients,
        }
        return newCocktail
    } else {
        return {};
    }
}