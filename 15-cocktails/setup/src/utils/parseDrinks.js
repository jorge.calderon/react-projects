export const prepareDrinksTostate = (drinks) => {
    if (drinks) {
        const newCocktails = drinks.map((item) => {
            const {
                idDrink,
                strDrink,
                strDrinkThumb,
                strAlcoholic,
                strGlass,
            } = item

            return {
                id: idDrink,
                name: strDrink,
                image: strDrinkThumb,
                info: strAlcoholic,
                glass: strGlass,
            }
        })
        return newCocktails;
    }

    return []
}