import Loading from '../components/Loading'
import { useParams, useNavigate, Link } from 'react-router-dom'
import { useCocktailDetails } from '../hooks/useCocktailDetails';

export const SingleCocktail = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { loading, cocktail } = useCocktailDetails(id, navigate)

  if (loading) {
    return <Loading />
  }

  const { name, image, category, info, glass, instructions, ingredients } = cocktail

  return (
    <section className='section cocktail-section'>
      <Link className="btn btn-primary" to={"/"}>Back home</Link>
      <h2 className="section-title">{name}</h2>
      <div className="drink">
        <img src={image} alt={name} />
        <div className="drink-info">
          <p>
            <span className='drink-data'>name :</span> {name}
          </p>
          <p>
            <span className='drink-data'>category :</span> {category}
          </p>
          <p>
            <span className='drink-data'>info :</span> {info}
          </p>
          <p>
            <span className='drink-data'>glass :</span> {glass}
          </p>
          <p>
            <span className='drink-data'>instructons :</span> {instructions}
          </p>
          <p>
            <span className='drink-data'>ingredients :</span>
            {ingredients.map((item, index) => item ? <span key={index}> {item}</span> : null)}
          </p>
        </div>
      </div>
    </section>
  )
}

