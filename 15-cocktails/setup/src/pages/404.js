import React from 'react'
import { Link } from 'react-router-dom'

export const NotFound = () => {
    return (
        <section className="error-page section">
            <div className="error-container">
                <h1>404 page not found</h1>
                <Link className="btn btn-primary" to={"/"}>
                    Back to home
                </Link>
            </div>

        </section>
    )
}

