export { NotFound } from './404';
export { About } from './About'
export { Error } from './Error'
export { Home } from './Home'
export { SingleCocktail } from './SingleCocktail'