import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { About, Error, Home, NotFound, SingleCocktail } from './pages'
import Navbar from './components/Navbar'

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/about' element={<About />} />
        <Route path='/cocktail/:id' element={<SingleCocktail />} />
        <Route path='/error' element={<Error />} />
        <Route path='*' element={<NotFound />} />
      </Routes>

    </Router>
  )
}

export default App
