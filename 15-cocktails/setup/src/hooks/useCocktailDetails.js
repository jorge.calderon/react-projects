import { useState, useCallback, useEffect } from 'react';
import { parseDetails } from '../utils/parseDetails'
import { API } from '../configuration/api';

export const useCocktailDetails = (id, navigate) => {

    const [loading, setLoading] = useState(true);
    const [cocktail, setCocktail] = useState({});


    const getCocktail = useCallback(async () => {
        try {
            setLoading(true);

            const url = new URL(API.LOOKUP_ENDPOINT);
            url.search = new URLSearchParams({ i: id }).toString();

            const resp = await fetch(url);
            const { drinks } = await resp.json();

            const cocktail = parseDetails(drinks)
            setCocktail(cocktail);
        } catch (error) {
            console.log(error)
            navigate('/error');
        } finally {
            setLoading(false)
        }
    }, [id, navigate])

    useEffect(() => {
        getCocktail();
    }, [getCocktail]);

    return { cocktail, loading }
}