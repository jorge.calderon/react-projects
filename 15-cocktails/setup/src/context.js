import React, { useState, useContext, useCallback, useEffect } from 'react'
import { API } from './configuration/api'
import { prepareDrinksTostate } from './utils/parseDrinks'

const AppContext = React.createContext()

const AppProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [cocktails, setCocktails] = useState([]);

  const getDrinks = useCallback(async (searchTerm) => {
    setLoading(true);
    try {
      const url = new URL(API.SEARCH_ENDPOINT);
      url.search = new URLSearchParams({ s: searchTerm }).toString();
      const resp = await fetch(url);
      const data = await resp.json();
      const cocktails = prepareDrinksTostate(data.drinks)
      setCocktails(cocktails)
    } catch (error) {
      console.log(error);
      setCocktails([]);
    } finally {
      setLoading(false)
    }
  }, [])

  useEffect(() => {
    getDrinks("")
  }, [getDrinks])

  return <AppContext.Provider value={{ loading, getDrinks, cocktails }}>{children}</AppContext.Provider>
}

export const useGlobalContext = () => useContext(AppContext)


export { AppContext, AppProvider }
