import React, { useRef, useEffect } from 'react'
import { useGlobalContext } from '../context'

const SearchForm = () => {
  const { getDrinks } = useGlobalContext();
  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <section className="section search">
      <form className="search-form" onSubmit={(e) => { e.preventDefault() }} >
        <div className="form-control">
          <label htmlFor="name">Search your favorite cocktail</label>
          <input
            autoComplete="off"
            type="text"
            name="name"
            id="name"
            ref={inputRef}
            onChange={({ target }) => getDrinks(target.value)}
          />
        </div>
      </form>
    </section>
  )
}

export default SearchForm
