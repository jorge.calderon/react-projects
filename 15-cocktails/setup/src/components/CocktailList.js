import React from 'react'
import Cocktail from './Cocktail'
import Loading from './Loading'
import { useGlobalContext } from '../context'

const CocktailList = () => {

  const { cocktails, loading } = useGlobalContext();

  if (loading) {
    return <Loading />
  }

  return (
    <div className="section">
      <h2 className="section-title">
        {
          cocktails.length === 0 ? "No Cocktails Matched Your Search Criteria" : "Cocktails"
        }
      </h2>
      <div className="cocktails-center">
        {
          cocktails.map((cocktail) => (<Cocktail key={cocktail.id} {...cocktail} />))
        }
      </div>
    </div>
  )
}

export default CocktailList
