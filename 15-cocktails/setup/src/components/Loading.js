import React from 'react'

const Loading = () => {
    return (
        <div className="loader" role="status" />
    )
}

export default Loading
