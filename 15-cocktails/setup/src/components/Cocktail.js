import React from 'react'
import { Link, useNavigate } from 'react-router-dom'

const Cocktail = ({ image, name, glass, id, info }) => {

  const navigate = useNavigate();

  return (
    <article className='cocktail'>
      <div className="img-container">
        <img src={image} alt={name} onClick={() => navigate(`/cocktail/${id}`)} />
      </div>
      <div className="cocktail-footer">
        <h3>{name}</h3>
        <h4>{glass}</h4>
        <p>{info}</p>
        <Link className="btn btn-primary btn-details" to={`/cocktail/${id}`} >Details</Link>
      </div>
    </article>
  )
}

export default Cocktail
