
export const API = {
    LOOKUP_ENDPOINT: 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php',
    SEARCH_ENDPOINT: "https://www.thecocktaildb.com/api/json/v1/1/search.php"
}

