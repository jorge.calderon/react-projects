import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import { drinks } from './data'
import { prepareDrinksTostate } from "../utils/parseDrinks";
import { BrowserRouter as Router } from 'react-router-dom'
import Cocktail from "../components/Cocktail";

const cocktail = prepareDrinksTostate(drinks)[0];

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: () => mockedUsedNavigate,
}));

describe('App test', () => {
    test('should render the information', () => {

        renderCocktail(cocktail);

        const { name, info, glass } = cocktail;

        expect(screen.getByText(name)).toBeInTheDocument();
        expect(screen.getByText(info)).toBeInTheDocument();
        expect(screen.getByText(glass)).toBeInTheDocument();
    });

    test('should create the correct link to see the cocktail detail', () => {

        renderCocktail(cocktail);

        const link = screen.getByRole("link");
        expect(link.textContent).toEqual("Details");
        expect(link.href).toContain("/cocktail/15997");
    });


    test('should redirect to the details page when the image is clicked', () => {

        renderCocktail(cocktail);

        const image = screen.getByAltText(cocktail.name);
        userEvent.click(image)

        expect(mockedUsedNavigate).toHaveBeenCalledWith(`/cocktail/${cocktail.id}`);
        expect(mockedUsedNavigate).toHaveBeenCalledTimes(1);

    });
});

const renderCocktail = (props) => {
    return render(
        <Router>
            <Cocktail {...props} />
        </Router>
    );
}