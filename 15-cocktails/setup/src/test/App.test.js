import { render, screen, waitForElementToBeRemoved } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { AppProvider } from './../context'
import "@testing-library/jest-dom";
import App from '../App';
import { drinks } from './data'
import { prepareDrinksTostate } from "../utils/parseDrinks";
import { parseDetails } from "../utils/parseDetails";

describe('App test', () => {

    let originalFetch;

    beforeEach(() => {
        originalFetch = global.fetch;
        global.fetch = jest.fn(() => Promise.resolve({
            json: () => Promise.resolve({ drinks })
        }));
    });

    afterEach(() => {
        global.fetch = originalFetch;
    });

    test('should render the information from the api', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByRole("status"));
        const parsedDrinks = prepareDrinksTostate(drinks);

        parsedDrinks.forEach(({ name, info, glass }) => {
            expect(screen.getByText(name)).toBeInTheDocument();
            expect(screen.getByText(info)).toBeInTheDocument();
            expect(screen.getByText(glass)).toBeInTheDocument();
        })
    });

    test('should show details of one cocktail', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByRole("status"));

        const buttons = screen.getAllByRole("link", { name: "Details" })
        const lastButton = buttons[buttons.length - 1];

        userEvent.click(lastButton);

        await waitForElementToBeRemoved(() => screen.getByRole("status"));
        const { name, info, category, glass, instructions, ingredients } = parseDetails(drinks)

        expect(screen.getAllByAltText(name)).toHaveLength(1);
        expect(screen.getByText(info)).toBeInTheDocument();
        expect(screen.getByText(category)).toBeInTheDocument();
        expect(screen.getByText(glass)).toBeInTheDocument();
        expect(screen.getByText(instructions)).toBeInTheDocument();

        ingredients.forEach((ingredient) => expect(screen.getByText(ingredient)).toBeInTheDocument());

    });


    test('should go to the about us section', async () => {

        render(<AppProvider><App /></AppProvider>);

        await waitForElementToBeRemoved(() => screen.getByRole("status"));

        const about = screen.getByText('about');

        userEvent.click(about);

        expect(screen.getByText("about us")).toBeInTheDocument();
        expect(screen.getByText("Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae repudiandae architecto qui adipisci in officiis, aperiam sequi atque perferendis eos, autem maiores nisi saepe quisquam hic odio consectetur nobis veritatis quasi explicabo obcaecati doloremque? Placeat ratione hic aspernatur error blanditiis?")).toBeInTheDocument();
    });

});