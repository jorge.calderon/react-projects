import { createContext } from 'react'
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import { drinks } from './data'
import { prepareDrinksTostate } from "../utils/parseDrinks";
import SearchForm from "../components/SearchForm";

const cocktail = prepareDrinksTostate(drinks)[0];

const MockAuthContext = createContext();

const mockedGetDrinks = jest.fn()

jest.mock("../context.js", () => ({
    __esModule: true,
    useGlobalContext: () => ({
        getDrinks: mockedGetDrinks
    })
}));


describe('App test', () => {
    test('should render the information', () => {

        renderForm(cocktail);
        const input = screen.getByRole("textbox");
        userEvent.type(input, "rum");
        expect(mockedGetDrinks).toHaveBeenCalledTimes(3);

        expect(mockedGetDrinks.mock.calls).toEqual([
            ["r"],  // First call
            ["ru"],  // Second call
            ["rum"],  // Second call
        ]);
    });
});

const renderForm = () => {
    return render(
        <MockAuthContext.Provider value={{}}>
            <SearchForm />
        </MockAuthContext.Provider>
    );
}