import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from '../App';
import reviews from '../data'

describe('App test', () => {

    test('should render the information of the first review', () => {

        render(<App />);

        expect(screen.getByText("Our Reviews")).toBeInTheDocument()

        const firstReview = reviews[0];

        const { name, job, text } = firstReview
        expect(screen.getByText(name)).toBeInTheDocument()
        expect(screen.getByText(job)).toBeInTheDocument()
        expect(screen.getByText(text)).toBeInTheDocument()
    })

    test('should show the next review when the next button is clicked', () => {

        render(<App />);

        expect(screen.getByText("Our Reviews")).toBeInTheDocument()

        const prevButton = screen.getByRole("button", { name: "next" })
        userEvent.click(prevButton);

        const nextReview = reviews[1];

        const { name, job, text } = nextReview;

        expect(screen.getByText(name)).toBeInTheDocument()
        expect(screen.getByText(job)).toBeInTheDocument()
        expect(screen.getByText(text)).toBeInTheDocument()

    })

    test('should show the last review when the previous button is clicked and the first review is showed', () => {

        render(<App />);

        expect(screen.getByText("Our Reviews")).toBeInTheDocument()

        const prevButton = screen.getByRole("button", { name: "previous" })
        userEvent.click(prevButton);

        const lastReview = reviews[reviews.length - 1];

        const { name, job, text } = lastReview;

        expect(screen.getByText(name)).toBeInTheDocument()
        expect(screen.getByText(job)).toBeInTheDocument()
        expect(screen.getByText(text)).toBeInTheDocument()

    })
})