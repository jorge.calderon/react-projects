import React from 'react';
import { useReviews } from './hooks/useReviews'
import { FaChevronLeft, FaChevronRight, FaQuoteRight } from 'react-icons/fa';

const Review = () => {

  const { getRandomReview, handleNext, handlePrev, review } = useReviews()
  const { image, name, job, text } = review


  return (
    <article className="review">
      <div className="img-container">
        <img className="person-img" src={image} alt={name} />
        <span className="quote-icon">
          <FaQuoteRight />
        </span>
      </div>
      <h4 className="author">{name}</h4>
      <div className="job">{job}</div>
      <div className="info">{text}</div>
      <div className="button-container">
        <button className="prev-btn" onClick={handlePrev} aria-label="previous">
          <FaChevronLeft />
        </button>
        <button className="next-btn" onClick={handleNext} aria-label="next">
          <FaChevronRight />
        </button>
      </div>
      <button className="random-btn" onClick={getRandomReview}>Surprise me</button>
    </article>
  );
};

export default Review;
