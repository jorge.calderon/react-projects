import { useState } from 'react';
import people from './../data';

export const useReviews = () => {
    const [active, setActive] = useState(0);

    const handleNext = () => {
        let nextActive = active + 1;
        if (nextActive > people.length - 1) {
            nextActive = 0;
        }
        setActive(nextActive)
    }

    const handlePrev = () => {
        let prevActive = active - 1;
        if (prevActive < 0) {
            prevActive = people.length - 1;
        }
        setActive(prevActive)
    }

    const getRandomReview = () => setActive(Math.floor(Math.random() * people.length));


    return {
        handleNext,
        handlePrev,
        getRandomReview,
        review: people[active]

    }
}