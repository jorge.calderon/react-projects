import React, { useState, useRef, useEffect } from 'react'
import { FaBars } from 'react-icons/fa'
import { links, social } from './data'
import logo from './logo.svg'

const Navbar = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [linksHeight, setLinksHeight] = useState(0)
  const navRef = useRef(null)

  const toggle = () => setShowMobileMenu(!showMobileMenu);

  useEffect(() => {
    if (showMobileMenu) {
      setLinksHeight(navRef.current.getBoundingClientRect().height)
    } else {
      setLinksHeight(0)
    }
  }, [showMobileMenu])


  return <nav ref={navRef}>
    <div className="nav-center">
      <div className="nav-header">
        <img src={logo} alt="logo" />
        <button className="nav-toggle" onClick={() => { toggle() }}>
          <FaBars />
        </button>
      </div>

      <div className="links-container" style={{ height: showMobileMenu ? `calc(100vh - ${linksHeight}px)` : linksHeight }}>
        <ul className="links">
          {
            links.map(({ url, id, text }) => (
              <li key={id}>
                <a href={url}>{text}</a>
              </li>
            ))
          }

        </ul>
      </div>

      <ul className="social-icons">
        {
          social.map(({ icon, id, url }) => (
            <li key={id}>
              <a href={url}>{icon}</a>
            </li>))
        }
      </ul>
    </div>
  </nav>
}

export default Navbar
