import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import App from './../App'

describe('App test', () => {

    test('should render one paragraph of lorem ipsum', () => {

        render(<App />);

        const generateButton = screen.getByRole("button", { name: "Generate" });

        userEvent.click(generateButton);
        expect(screen.getAllByRole("textbox")).toHaveLength(1)

    });

    test('should render five paragraphs of lorem ipsum', () => {

        render(<App />);

        const input = screen.getByRole("spinbutton", { name: "paragraphs" });
        userEvent.type(input, "{backspace}5");

        const generateButton = screen.getByRole("button", { name: "Generate" });
        userEvent.click(generateButton);

        expect(screen.getAllByRole("textbox")).toHaveLength(5)


    });

})