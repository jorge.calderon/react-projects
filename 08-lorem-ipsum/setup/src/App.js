import React, { useState } from 'react';
import { loremIpsum } from "lorem-ipsum";

function App() {
  const [numberOfParagraphs, setNumberOfParagraphs] = useState(1);
  const [paragraphs, setParagraphs] = useState([])

  const handleSubmit = (e) => {
    e.preventDefault();
    const paragraphs = [];

    for (let i = 0; i < numberOfParagraphs; i++) {
      paragraphs.push(loremIpsum({
        count: 10,
        format: "plain",
        paragraphLowerBound: 3,
        paragraphUpperBound: 7,
        random: Math.random,
        sentenceLowerBound: 5,
        sentenceUpperBound: 15,
        units: "sentences",
      }))
    }
    setParagraphs(paragraphs)

  }

  return (
    <section className="section-center">
      <h3>Tired of boring lorem ipsum?</h3>
      {
        JSON.stringify(numberOfParagraphs)
      }
      <form className='lorem-form' onSubmit={handleSubmit}>
        <label htmlFor="paragraphs">paragraphs</label>
        <input type="number" name="paragraphs" id='paragraphs' value={numberOfParagraphs} min={1} onChange={(e) => { setNumberOfParagraphs(e.target.value) }} />
        <button className='btn'>Generate</button>
      </form>
      <div className="lorem-text">
        {
          paragraphs.map((p, i) => <p key={i} role="textbox">{p}</p>)
        }
      </div>
    </section>
  )
}

export default App;
