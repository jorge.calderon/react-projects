import { useState, useEffect } from 'react';
import { prepareUrl, parsePhoto } from '../utils/';

export function useInfiniteScroll(query, pageNumber) {
    const [loading, setLoading] = useState(true);
    const [photos, setPhotos] = useState([]);

    const fetchPhotos = async (query, pageNumber) => {
        setLoading(true);
        try {
            const url = prepareUrl(query, pageNumber)
            const resp = await fetch(url);
            const data = await resp.json();
            const parsedData = query ? data.results.map(parsePhoto) : data.map(parsePhoto)
            setPhotos((prevPhotos) => query && pageNumber === 1 ? parsedData : [...new Set([...prevPhotos, ...parsedData])]);
        } catch (error) {
            console.log(error);
            setPhotos([]);
            alert('Something went wrong, please try again')
        } finally {
            setLoading(false)
        }
    }

    useEffect(() => {
        fetchPhotos(query, pageNumber);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageNumber])


    return { loading, photos, fetchPhotos }
}