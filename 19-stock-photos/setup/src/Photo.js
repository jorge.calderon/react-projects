import React from 'react'

const Photo = ({ img, author, likes, auhorImg, href, alt }) => {
  return (
    <article className="photo">
      <img src={img} alt={alt} />
      <div className="photo-info">
        <div>
          <h4>{author}</h4>
          <p>{likes} likes</p>
        </div>
        <a href={href}>
          <img src={auhorImg} className="user-img" alt="img" />
        </a>
      </div>
    </article>
  )
}

export default Photo
