import React, { useState, useRef, useCallback } from 'react'
import { FaSearch } from 'react-icons/fa'
import { useInfiniteScroll } from './hooks/useInfiniteScroll';
import Photo from './Photo'

const FIRST_PAGE = 1;

function App() {
  const [query, setQuery] = useState("")
  const [pageNumber, setPageNumber] = useState(1);

  const { photos, loading, fetchPhotos } = useInfiniteScroll(query, pageNumber);

  const observer = useRef();

  const lastPhotoRef = useCallback(node => {
    if (loading) return
    if (observer.current) observer.current.disconnect()
    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting) {
        setPageNumber(prevPageNumber => prevPageNumber + 1)
      }
    })
    if (node) observer.current.observe(node)
  }, [loading])


  const handleSubmit = (e) => {
    e.preventDefault();
    if (pageNumber === 1) {
      fetchPhotos(query, pageNumber)
    }
    setPageNumber(FIRST_PAGE);
  }

  return (
    <main>
      <section className="search">
        <form className="search-form" onSubmit={handleSubmit}>
          <input
            type="text"
            className="form-input"
            value={query}
            onChange={({ target: { value } }) => { setQuery(value) }}
            placeholder="search"
          />
          <button type="submit" className="submit-btn">
            <FaSearch />
          </button>
        </form>
      </section>
      <section className="photos">
        <div className="photos-center">

          {
            photos.length === 0 ? <p>No results</p> :
              photos.map((photo, i) => (
                photos.length === i + 1
                  ?
                  <div style={{ backgroundColor: 'red' }} key={i} ref={lastPhotoRef}>
                    <Photo {...photo} />
                  </div>
                  :
                  <Photo key={i} {...photo} />
              ))
          }
        </div>
        {loading && <h2 className="loading">Loading...</h2>}
      </section>
    </main>
  )
}

export default App
