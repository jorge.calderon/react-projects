import { clientID, mainUrl, searchUrl } from '../configuration/unsplash.api'

export const prepareUrl = (query, pageNumber) => {
    let url;
    const urlPage = `&page=${pageNumber}`;
    const urlQuery = `&query=${query}`;

    if (query) {
        url = `${searchUrl}${clientID}${urlPage}${urlQuery}`;
    } else {
        url = `${mainUrl}${clientID}${urlPage}`;
    }

    return url;
}