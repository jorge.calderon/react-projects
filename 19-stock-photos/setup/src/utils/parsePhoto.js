export const parsePhoto = ({
    urls: { regular },
    alt_description,
    likes,
    user: {
        name,
        portfolio_url,
        profile_image: { medium },
    },
}) => {

    return {
        img: regular,
        alt: alt_description,
        author: name,
        likes: likes,
        auhorImg: medium,
        href: portfolio_url
    }

}